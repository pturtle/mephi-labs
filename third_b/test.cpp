#include <gtest/gtest.h>
#include "arr.h"

using namespace nm;
TEST(a, b) {
  std::string line = "kek";
  std::string line1 = "kekw";
  std::string* ptr = new std::string [2];
  ptr[0] = line;
  ptr[1] = line1;

  Arr* arr = new Arr();
  Arr* arr1 = new Arr(line);
  Arr* arr2 = new Arr(ptr, 2);

  ASSERT_NO_THROW(arr);
  ASSERT_NO_THROW(arr1);
  ASSERT_NO_THROW(arr2);
}

TEST(b, c) {
  std::string line = "lol";

  Arr* arr = new Arr(line);
  EXPECT_EQ(arr->len(), 1);
}

TEST(e, d) {
  std::string* ptr = new std::string [4];
  ptr[0]="alksjd";
  ptr[1]="adlfg";
  ptr[2]="wqjd";
  ptr[3]="ldfjg";

  Arr* arr = new Arr(ptr, 4);
  ASSERT_NO_THROW(std::cout << *arr);

  arr->sort();

  EXPECT_EQ(strcmp((*arr)[3], "ldfjg"), false);

  (*arr)("a");

  EXPECT_EQ(strcmp((*arr)[1], "adlfg"), false);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
