#include <iostream>
#include <set>
#include <cassert>
#include <cstring>
#include <ostream>
#include "arr.h"
#define MAX_WORDLEN 20
#define MAX_ARRLEN 20

namespace nm {
    Arr::Arr() {
      size_arr = 0;
      ptr = new char* [1]();
      ptr[0] = 0;
    }
    Arr::Arr(std::string buf) {
      ptr = new char* [1]();
      ptr[0] = new char[buf.length()+0x10]();
      strncpy(ptr[0], buf.c_str(), buf.length());
      size_arr++;
    }
    Arr::Arr(std::string* buf, int size) {
      int c = 0, t;
      ptr = new char* [size]();
      size_arr = size;
      for (int i = 0; i < size; i++) {
        ptr[i] = NULL;
      }
      for (int i = 0; i < size; i++) {
        t = 0;
        for (int j = 0; j < i; j++) {
          if (ptr[j] && !strcmp(ptr[j], buf[i].c_str())) {
            t = -1;
            c++;
            break;
          }
        }
        if (t == -1) {
          continue;
        }
        ptr[i-c] = new char[buf[i].length()+0x10]();
        strncpy(ptr[i-c], buf[i].c_str(), buf[i].length());
      }
      size_arr -= c;
    }
    Arr::Arr(const Arr &arr) {
      size_arr = arr.size_arr;
      ptr = new char* [size_arr]();
      for (int i = 0; i < size_arr; i++) {
        ptr[i] = new char[strlen(arr.ptr[i])+0x10];
        strcpy(ptr[i], arr.ptr[i]);
      }
    }
    Arr::Arr(const Arr &&arr) {
      size_arr = arr.size_arr;
      ptr = new char* [size_arr]();
      for (int i = 0; i < size_arr; i++) {
        ptr[i] = arr.ptr[i];
      }
    }
    Arr::~Arr() {
      for (int i = 0; i < size_arr; i++) {
        if (ptr[i]) delete[] ptr[i];
      }
      delete[] ptr;
    }
    int Arr::len() {
      return size_arr;
    }
    int Arr::find(const char* buf) {
      for (int i = 0; i < size_arr; i++) {
        if (ptr[i] && !strcmp(ptr[i], buf)) {
          return i;
        }
      }
      return -1;
    }
    const char* Arr::str(int line) {
      if (ptr[line] != 0) {
        return ptr[line];
      } else {
        throw "Invalid line";
        return "";
      }
    }
    int compare(char* str1, char* str2) {
      int size1 = strlen(str1);
      int size2 = strlen(str2);
      for (int i = 0; i < (size1 < size2 ? size1 : size2); i++) {
        if (str1[i] == str2[i]) {
          continue;
        }
        if (str1[i] < str2[i]) {
          return 1;
        } else {
          return 2;
        }
      }
      return (size1 < size2 ? 1 : 2);
    }
    Arr& Arr::sort() {
      char* temp;
      for (int i = 0; i < size_arr; i++) {
        for (int j = i+1; j < size_arr; j++) {
          if (compare(ptr[i], ptr[j])==2) {
            temp = ptr[i];
            ptr[i] = ptr[j];
            ptr[j] = temp;
          }
        }
      }
      return *this;
    }
    std::ostream& operator<< (std::ostream &out, const Arr &arr) {
      for (int i = 0; i < arr.size_arr; i++){
        out << "[" << i+1 << "] " << arr.ptr[i] << std::endl;
      }
      return out;
    }
    std::istream& operator>> (std::istream &in, Arr &arr) {
      std::string buf;
      in >> buf;
      for (int i = 0; i < arr.size_arr; i++) {
        if (arr.ptr[i] && !strcmp(arr.ptr[i], buf.c_str())) {
          return in;
        }
      }
      char** ptr1 = new char* [arr.size_arr+1]();
      for (int i = 0; i < arr.size_arr; i++) {
        ptr1[i] = new char[strlen(arr.ptr[i])+0x10]();
        strncpy(ptr1[i], arr.ptr[i], strlen(arr.ptr[i]));
        delete[] arr.ptr[i];
      }
      ptr1[arr.size_arr] = new char[buf.length()+0x10]();
      strncpy(ptr1[arr.size_arr], buf.c_str(), buf.length()+0x10);
      delete arr.ptr;
      arr.ptr = ptr1;
      arr.size_arr++;
      return in;
    }
    Arr &Arr::operator += (std::string buf) {
      for (int i = 0; i < size_arr; i++) {
        if (ptr[i] && !strcmp(ptr[i], buf.c_str())) {
          return *this;
        }
      }
      char** ptr1 = new char* [size_arr+1]();
      for (int i = 0; i < size_arr; i++) {
        ptr1[i] = new char[strlen(ptr[i])+0x10]();
        strncpy(ptr1[i], ptr[i], strlen(ptr[i]));
        delete[] ptr[i];
      }
      ptr1[size_arr] = new char[buf.length()+0x10]();
      strncpy(ptr1[size_arr], buf.c_str(), buf.length()+0x10);
      delete ptr;
      ptr = ptr1;
      size_arr++;
      return *this;
    }
    Arr &Arr::operator()(const char* buf) {
      for (int i = 0; i < size_arr; i++) {
        if (strncmp(buf, ptr[i], strlen(buf)) != 0) {
          delete ptr[i];
          for (int j = i+1; j < size_arr; j++) {
            ptr[j-1] = ptr[j];
          }
          i--;
          size_arr--;
        }
      }
      return *this;
    }
    Arr &Arr::operator=(Arr &arr) {
      if (&arr == this)
			   return *this;
      for (int i = 0; i < size_arr; i++) {
        delete[] ptr[i];
      }
      delete ptr;
      ptr = new char* [arr.size_arr]();
      size_arr = arr.size_arr;
      for (int i = 0; i < size_arr; i++) {
        ptr[i] = new char[strlen(arr.ptr[i])+0x10];
        strcpy(ptr[i], arr.ptr[i]);
      }
      return *this;
    }
    Arr &Arr::operator=(Arr &&arr) {
      if (&arr == this)
			   return *this;
      for (int i = 0; i < size_arr; i++) {
        delete[] ptr[i];
      }
      delete ptr;
      ptr = new char* [arr.size_arr]();
      size_arr = arr.size_arr;
      for (int i = 0; i < size_arr; i++) {
        ptr[i] = arr.ptr[i];
      }
      return *this;
    }
    char* &Arr::operator[](int index) {
      return ptr[index-1];
    }
  };
