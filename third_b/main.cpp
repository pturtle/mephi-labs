#include <iostream>
#include <cstring>
#include "arr.h"
using namespace std;
using namespace nm;

bool is_number(const std::string& s){
    try {
        std::stod(s);
    } catch(...) {
        return false;
    }
    return true;
}

bool get_input(string& line) {
  line.clear();
  if (!(cin >> line)) {
    throw invalid_argument("Invalid input");
    return 1;
  }
  return 0;
}

void menu() {
  cout << "----------------------" << endl;
  cout << "1. Initialize array" << endl;
  cout << "2. Print array" << endl;
  cout << "3. Get element by index" << endl;
  cout << "4. Find element" << endl;
  cout << "5. Add element" << endl;
  cout << "6. Add element via stream" << endl;
  cout << "7. Sort array" << endl;
  cout << "8. Make an array starting with..." << endl;
  cout << "9. Exit" << endl;
  cout << "----------------------" << endl;
  cout << "Go on with your choice:" << endl;
}

int main() {
  Arr* arr = NULL;
  int temp = 0;
  std::string line;
  for (;;) {
    menu();
    try{
      get_input(line);
    } catch(const std::invalid_argument &err) {
      cout << "(error) Invalid input: " << err.what() << endl;
      continue;
    }
    if (!is_number(line)){
      continue;
    }
    if (!arr && (stoi(line)!=1 && stoi(line)!=9)) {
      continue;
    }
    switch (stoi(line)) {
      case 1:
        cout << "a) By default" << endl;
        cout << "b) With one word" << endl;
        cout << "c) With few words" << endl;
        line.clear();
        cin >> line;
        switch (line.c_str()[0]) {
          case 'a':
            arr = new Arr();
            break;
          case 'b':
            cout << "Waiting for your word>> ";
            try {
              get_input(line);
            } catch(const std::invalid_argument &err) {
              cout << "(error) Invalid input: " << err.what() << endl;
              break;
            }
            arr = new Arr(line);
            break;
          case 'c':
            cout << "Input number of words:" << endl;
            try {
              get_input(line);
            } catch(const std::invalid_argument &err) {
              cout << "(error) Invalid input: " << err.what() << endl;
              break;
            }
            temp = stoi(line);
            if (temp < 1) {
              break;
            }
            string* ptr = new string [temp];
            cout << "Waiting for your " << temp << " words" << endl;
            for (int i = 0; i < temp; i++) {
              ptr[i] = string();
              cin >> ptr[i];
            }
            arr = new Arr(ptr, temp);
            delete[] ptr;
            break;
        }
        break;
      case 2:
        if (arr) cout << *arr;
        break;
      case 3:
        if (!arr) break;
        cout << "Input index>> ";
        try {
          get_input(line);
        } catch(const std::invalid_argument &err) {
          cout << "(error) Invalid input: " << err.what() << endl;
          break;
        }
        temp = stoi(line);
        if (temp > arr->len() || temp < 1) {
          break;
        } else {
          cout << "[+] Your word is " << (*arr)[temp] << endl;
        }
        break;
      case 4:
        cout << "What to find>> ";
        try {
          get_input(line);
        } catch(const std::invalid_argument &err) {
          cout << "(error) Invalid input: " << err.what() << endl;
          break;
        }
        temp = arr->find(line.c_str());
        temp == -1 ? cout << "[-] Not found" << endl : cout << endl << "[+] Index "<< temp+1 << endl;
        break;
      case 5:
        cout << "What to add>> ";
        try {
          get_input(line);
        } catch(const std::invalid_argument &err) {
          cout << "(error) Invalid input: " << err.what() << endl;
          break;
        }
        *arr += line;
        break;
      case 6:
        cout << "Waiting for your words>> ";
        cin >> *arr;
        break;
      case 7:
        arr->sort();
        cout << "Sorting done." << endl;
        break;
      case 8:
        cout << "Input the start you want>> ";
        try {
          get_input(line);
        } catch(const std::invalid_argument &err) {
          cout << "(error) Invalid input: " << err.what() << endl;
          break;
        }
        (*arr)(line.c_str());
        break;
      case 9:
        exit(0);
    }
  }
}
