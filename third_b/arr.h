#include <ostream>
#ifndef OOP3_ARR_H
#define OOP3_ARR_H
#define MAX_WORDLEN 20
#define MAX_ARRLEN 20

namespace nm {
  class Arr {
  private:
    int size_arr = 0;
    char** ptr;
  public:
    Arr();
    Arr(std::string buf);
    Arr(std::string* buf, int size);
    Arr(const Arr &&arr);
    Arr(const Arr &arr);
    ~Arr();
    int len();
    int find(const char* buf);
    const char* str(int line);
    Arr& sort();
    friend std::ostream& operator<< (std::ostream &out, const Arr &arr);
    friend std::istream& operator>> (std::istream &out, Arr &arr);
    Arr &operator += (std::string buf);
    Arr &operator () (const char* buf);
    Arr &operator=(Arr &arr);
    Arr &operator=(Arr &&arr);
    char* &operator[](int index);
  };
}
#endif
