#ifndef OOP4_ROOM_H
#define OOP4_ROOM_H
#include <string>
namespace oop4 {
    /*!
    \brief Абстрактный базовый класс
    \author pturtle
    \version 1.0
    \date февраль 2021 года
    \warning невозможно создать объект данного класса, не используя наследников

    Содержит чисто виртуальные функции
    */
    class Apartment {
    private:
      int num; ///< номер комнаты, важен также и для контейнера, число больше нуля.
      bool busy; ///<  занят ли номер
      int people; ///<  количество людей в номере
      int cost_per_day; ///<  стоимость номера за день
      std::string type; ///<  тип номера (Lux, Standart Multiseat)
    protected:
      /*!
      Выставляет переменные при первом создании (искусственая замена конструктора)
      \param[in] n - номер
      \param[in] c - цена
      \param[in] str - тип
      */
      Apartment& Set_first(int n, int c, const std::string&);
      /*!
      Выставляет значение флага busy
      \param[in] не нуждается в комментарии

      */
      Apartment& Set_busy(bool);
      /*!
      Выставляет значение количества людей
      \param[in] количество людей

      */
      Apartment& Set_people(int);
    public:
      friend std::ostream& operator << (std::ostream& out, const Apartment&);
      /*!
      Геттер
      \return Возвращает флаг busy
      */
      bool Get_busy() const;
      /*!
      Геттер
      \return Возвращает номер комнаты
      */
      int Get_number() const;
      /*!
      Геттер
      \return Возвращает количество людей в номере
      */
      int Get_people() const;
      /*!
      \return Возвращает тип номера
      */
      std::string Get_type();
      /*!
      Чисто виртуальная функция реализованная в классах наследниках, функция занятия номера
      \param[in] d - дата
      \param[in] t - время
      \param[in] p - количество человек
      */
      virtual Apartment& Take(int d, int t, int p) = 0;
      /*!
      Чисто виртуальная функция реализованная в классах наследниках, функция освобождения номера, принимает в качестве аргумента для номеров класса Lux и Standart колличество человек для выселения, а для Multiseat это поле используется для выселение человека по его id
      \param[in] p - количество человек
      */
      virtual Apartment& Checkout(int p) = 0;
      /*
      Чисто виртуальная функция, вывод полной информации о номере
      */
      virtual std::ostream& show(std::ostream&) const = 0;
    };
}
#endif
