#include "Apartment.h"
#include <iostream>
#include <string>
namespace oop4 {
    std::ostream& operator << (std::ostream& out, const Apartment& apart) {
        out << "Apartment num >> ";
        out << " " << apart.num << "\n";
        out << "Apartment type >> ";
        out << " " << apart.type<<"\n";
        out << ((apart.busy == true) ? ("Apartment is busy\n") : ("Apartment is free\n"));
        out << "Apartment cost >> ";
        out << " " << apart.cost_per_day << "\n";
        apart.show(out);
        return out;
    }
    int Apartment::Get_number() const {
        return this->num;
    }
    Apartment &Apartment::Set_first(int room_number, int cost, const std::string& t) {
        this->num = room_number;
        this->cost_per_day = cost;
        this->busy = false;
        this->type = t;
        return *this;
    }
    Apartment &Apartment::Set_busy(bool b) {
        this->busy = 0;
        return *this;
    }
    bool Apartment::Get_busy() const {
        return this->busy;
    }
    int Apartment::Get_people() const {
        return this->people;
    }
    Apartment &Apartment::Set_people(int p) {
        this->people = p;
        return *this;
    }
    std::string Apartment::Get_type() {
        return this->type;
    }
}
