#include "../Apartment/Apartment.h"
#ifndef OOP4_MULTISEAT_H
#define OOP4_MULTISEAT_H

namespace oop4 {
    class Multiseat : public Apartment {
    private:
      struct humans {
        int number;
        int date;
        int time;
      };
      humans hum[4];
      int places;
      int free_places;
    public:
      Multiseat();
      Multiseat(int, int, int);
      Apartment& Take(int d, int t, int p) override;
      Apartment & Checkout(int p) override;
      std::ostream & show(std::ostream &) const override;
    };
}
#endif
