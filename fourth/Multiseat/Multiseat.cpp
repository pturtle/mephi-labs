#include <iostream>
#include "Multiseat.h"
namespace oop4 {
    Multiseat::Multiseat(int room_number, int cost, int p): places(p), free_places(p) {
      this->Set_first(room_number, cost, "Multiseat");
      this->Set_people(0);
      for (int i = 0; i < 4; i++){
        this->hum->time = 0;
        this->hum->date = 0;
        this->hum->number= 0;
      }
    }
    Apartment &Multiseat::Take(int date, int time, int people) {
      if (people <= free_places) {
        for (int i = 0; i < people; i++) {
          hum[i].number = i + 1;
          hum[i].date = date;
          hum[i].time = time;
        }
        free_places -= people;
        this->Set_busy(true);
        this->Set_people(places - free_places);
        if(this->Get_people() == places) {
            this->Set_busy(true);
        }
      } else {
        throw std::invalid_argument("Not enough place");
      }
      return *this;
    }
    Apartment &Multiseat::Checkout(int p) {
      bool is = false;
      for (int i = 0; i < places; i++) {
        if (hum[i].number == p) {
          hum[i].date = 0;
          hum[i].time = 0;
          is = true;
          break;
        }
      }
      if (!is)
        throw std::invalid_argument("No such man");
      this->Set_people(this->Get_people() - 1);
      this->free_places = free_places + 1;
      if (free_places == places)
        this->Set_busy(false);
      return *this;
    }
    std::ostream &Multiseat::show(std::ostream &out) const {
      out << "Amount of people >> ";
      out << this->Get_people()<<"\n";
      out << "Free place >> ";
      out << free_places <<"\n";
      out << "All places >> ";
      out << places << "\n";
      out << "Living in that apartment\n";
      for (int i = 0; i < places-free_places; i++) {
        out << "\nGuest number ";
        out << hum[i].number;
        out << " Living from >> ";
        out << hum[i].date<<"\n";
        out << " Living for >> ";
        out << hum[i].time << " days";
      }
      return out;
    }
    Multiseat::Multiseat() = default;
}
