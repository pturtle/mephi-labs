#include <iostream>
#include "Standart.h"
namespace oop4 {
    Standart::Standart(int room_number, int cost) {
      this->Set_first(room_number, cost, "Standart");
      this->Set_busy(false);
      date = 0;
      time = 0;
      this->Set_people(0);
    }
    std::ostream &Standart::show(std::ostream &out) const {
      out << "Amount of people ";
      out << this->Get_people() << "\n";
      out << " Living from -> ";
      out << date << "\n";
      out << "Living for -> ";
      out << time;
      return out;
    }
    Apartment &Standart::Take(int d, int t, int p) {
      this->Set_busy(true);
      date = d;
      time = t;
      this->Set_people(p);
      return *this;

    }
    Apartment &Standart::Checkout(int p) {
      this->Set_busy(false);
      this->Set_people(0);
      date = 0;
      time = 0;
      return *this;
    }
    Standart::Standart() = default;
}
