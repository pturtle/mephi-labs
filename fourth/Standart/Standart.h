#include "../Apartment/Apartment.h"
#ifndef OOP4_STANDART_H
#define OOP4_STANDART_H

namespace oop4 {
    class Standart : public Apartment {
    private:
       int date;
       int time;
    public:
        Standart();
        Standart(int, int);
        Apartment& Take(int d, int t, int p) override;
        Apartment& Checkout(int p) override;
        std::ostream& show(std::ostream &) const override;
    };
}
#endif
