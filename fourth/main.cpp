#include <iostream>
#include "Table/Table.h"
#include "Lux/Lux.h"
#include "Multiseat/Multiseat.h"
#include "dialog/dialog.h"

using namespace std;
using namespace oop4;
int main() {
    try {
        dialog();
    }
    catch (std::exception &ex) {
        std::cout << "Exception detected: " << ex.what() << std::endl;
    }
}
