#include <gtest/gtest.h>
#include "../Table/Table.h"
#include "../Apartment/Apartment.h"
#include "../Lux/Lux.h"
#include "../Standart/Standart.h"
#include "../Multiseat/Multiseat.h"

using namespace oop4;

TEST(a, b) {
  Apartment* lux = new Lux(3, 123);
  Apartment* standart = new Standart(4, 321);
  Apartment *multiseat = new Multiseat(1, 2, 3);

  ASSERT_NO_THROW(lux);
  ASSERT_NO_THROW(standart);
  ASSERT_NO_THROW(multiseat);
}

TEST(b, c) {
  Apartment* lux = new Lux(7, 228);
  Apartment* standart = new Standart(5, 1488);
  Apartment *multiseat = new Multiseat(3, 2, 1);

  ASSERT_NO_THROW(lux->Get_busy());
  ASSERT_NO_THROW(lux->Get_number());
  ASSERT_NO_THROW(lux->Get_people());
  ASSERT_NO_THROW(lux->Get_type());

  ASSERT_NO_THROW(standart->Get_busy());
  ASSERT_NO_THROW(standart->Get_number());
  ASSERT_NO_THROW(standart->Get_people());
  ASSERT_NO_THROW(standart->Get_type());

  ASSERT_NO_THROW(multiseat->Get_busy());
  ASSERT_NO_THROW(multiseat->Get_number());
  ASSERT_NO_THROW(multiseat->Get_people());
  ASSERT_NO_THROW(multiseat->Get_type());
}

TEST(d, e) {
  Table table;
  Apartment* lux = new Lux(10, 15);
  Apartment *multiseat = new Multiseat(1, 2, 3);

  table.Add(lux);
  table.Add(multiseat);

  ASSERT_NO_THROW(table.Find_element(10));
  ASSERT_NO_THROW(table.Find_element(1));

  ASSERT_NO_THROW(table.Show_element(1));
  ASSERT_NO_THROW(table.Show_element(10));

  ASSERT_NO_THROW(table.Del(10));
  EXPECT_THROW({
        try
        {
            table.Find_element(10);
        }
        catch(std::exception &ex)
        {
            EXPECT_STREQ("invalid number!", ex.what());
            throw;
        }
    }, std::exception);

  ASSERT_NO_THROW(table.QShow());
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
