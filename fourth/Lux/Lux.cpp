#include <iostream>
#include "Lux.h"
namespace oop4{
    Lux::Lux(int room_number, int cost) {
      this->Set_first(room_number, cost, "Lux");
      this->Set_people(0);
      time = 0;
      date = 0;
    }
    Apartment &Lux::Checkout(int p) {
      this->Set_people(0);
      this->Set_busy(false);
      this->time = 0;
      this->date = 0;
      return *this;
    }
    std::ostream &Lux::show(std::ostream &out) const {
      out << "Amount of people >> ";
      out << this->Get_people() << "\n";
      out << "Living from >> ";
      out << date << "\n";
      out << "Living for >> ";
      out << time;
      return out;
    }
    Apartment &Lux::Take(int d, int t, int p) {
      this->date = d;
      this->time = t;
      this->Set_busy(true);
      this->Set_people(p);
      return *this;
    }
}
