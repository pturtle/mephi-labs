#include "../Apartment/Apartment.h"

#ifndef OOP4_LUX_H
#define OOP4_LUX_H

namespace oop4 {
    class Lux: public Apartment {
    private:
        int date;
        int time;
    public:
        Lux(int n, int d);
        Apartment& Take(int d, int t, int p) override;
        Apartment& Checkout(int p) override;
        std::ostream &show(std::ostream &out) const override;
    };
}
#endif
