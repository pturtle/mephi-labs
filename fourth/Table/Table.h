#ifndef OOP4_TABLE_H
#define OOP4_TABLE_H
#include <map>
#include <vector>
#include "../Apartment/Apartment.h"
namespace oop4 {
    class Table {
    private:
        std::map<int,Apartment*> all;
    public:
        Table();
        Apartment* Find_element(int key);
        std::map<int, Apartment *>::iterator Find_it(int);
        void Display();
        Table &Add(Apartment *);
        void Show_element(int k);
        Table &Del(int);
        void QShow();
};
}
#endif
