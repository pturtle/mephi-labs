#include <iostream>
#include "../Table/Table.h"
#include "../Lux/Lux.h"

namespace oop4 {
    void Table::Display() {
       for (auto it = all.begin(); it != all.end(); ++it) {
            std::cout <<*it->second;
            std::cout<<std::endl;
            std::cout<<std::endl;
            std::cout<<std::endl;
        }
    }
    Table &Table::Add(Apartment *apart) {
        all.insert(std::make_pair(apart->Get_number(), apart));
        return *this;
    }
    void Table::Show_element(int key) {
        try {
            Apartment * find;
            find = Find_element(key);
            std::cout <<*find;
            std::cout <<"\n";
        }
        catch (std::exception &ex) {
            std::cout << "Exception detected: " << ex.what() << std::endl;
        }
    }
    Apartment *Table::Find_element(int key) {
        std::map<int,Apartment*> ::iterator it;
        it = all.find(key);
        if(it != all.end()){
            return it->second;
        }
        throw std::invalid_argument("invalid number!");
    }
    Table &Table::Del(int key) {
        try {
            // Apartment * find;
            // find = Find_element(key);
            all.erase(key);
        }
        catch (std::exception &ex) {
            std::cout << "Exception detected: " << ex.what() << std::endl;
        }
        return *this;
    }
    void Table::QShow() {
        std::cout<< "Num         Type";
        std::cout << "\n";
        for (auto it = all.begin(); it != all.end(); ++it) {
            std::cout << it->first;
            std::cout << "           ";
            std::cout << it->second->Get_type();
            std::cout << "\n";
        }
    }
    std::map<int, Apartment *>::iterator Table::Find_it(int key) {
        std::map<int, Apartment *>::iterator it;
        it = all.find(key);
        return it;
    }
    Table::Table() = default;
}
