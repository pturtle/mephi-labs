#include "arr.h"
#include <iostream>
#include <set>
#include <cassert>
#include <cstring>
#include <ostream>
#define MAX_WORDLEN 20
#define MAX_ARRLEN 20

namespace nm {
    Arr::Arr() {
      size_arr = 0;
      for (int i = 0; i < MAX_ARRLEN; i++) {
        std::cout << ptr[i] << std::endl;
      }
    }
    Arr::Arr(char* buf, int size) {
      if (size > MAX_WORDLEN) {
        size = MAX_WORDLEN;
      }
      strncpy(ptr[0], buf, size);
      ptr[0][MAX_WORDLEN-1] = 0;
      size_arr++;
    }
    Arr::Arr(char** buf, int size) {
      assert(size+1 < MAX_ARRLEN && "Size is not correct");
      int c = 0, t;
      for (int i = 0; i < size; i++) {
        t = 0;
        for (int j = 0; j < i; j++) {
          if (ptr[j] && !strcmp(ptr[j], buf[i])) {
            t = -1;
            c++;
            break;
          }
        }
        if (t == -1) {
          continue;
        }
        strncpy(ptr[i-c], buf[i], MAX_WORDLEN);
        ptr[i-c][MAX_WORDLEN-1] = 0;
      }
      size_arr += (size - c);
    }
    void wipe(char kek[MAX_WORDLEN]) {
      for (int i = 0; i < MAX_WORDLEN; i++) {
        kek[i] = 0;
      }
    }
    int Arr::len() {
      return size_arr;
    }
    int Arr::find(const char* buf) {
      for (int i = 0; i < size_arr; i++) {
        if (ptr[i] && !strcmp(ptr[i], buf)) {
          return i;
        }
      }
      return -1;
    }
    const char* Arr::str(int line) {
      if (ptr[line] != 0) {
        return ptr[line];
      } else {
        throw "Invalid line";
        return "";
      }
    }
    int compare(char* str1, char* str2) {
      int size1 = strlen(str1);
      int size2 = strlen(str2);
      for (int i = 0; i < (size1 < size2 ? size1 : size2); i++) {
        if (str1[i] == str2[i]) {
          continue;
        }
        if (str1[i] < str2[i]) {
          return 1;
        } else {
          return 2;
        }
      }
      return (size1 < size2 ? 1 : 2);
    }
    Arr& Arr::sort() {
      for (int i = 0; i < size_arr; i++) {
        for (int j = i+1; j < size_arr; j++) {
          if (compare(ptr[i], ptr[j])==2) {
            std::swap(ptr[i], ptr[j]);
          }
        }
      }
      return *this;
    }
    std::ostream& operator<< (std::ostream &out, const Arr &arr) {
      for (int i = 0; i < arr.size_arr; i++){
        out << "[" << i+1 << "] " << arr.ptr[i] << std::endl;
      }
      return out;
    }
    Arr &Arr::operator += (const char* buf) {
      assert(size_arr+1 < MAX_ARRLEN && "Invalid size");
      for (int i = 0; i < size_arr; i++) {
        if (ptr[i] && !strcmp(ptr[i], buf)) {
          return *this;
        }
      }
      strncpy(ptr[size_arr], buf, strlen(buf)>20 ? 20 : strlen(buf));
      ptr[size_arr][MAX_WORDLEN-1] = 0;
      size_arr++;
      return *this;
    }
    Arr &Arr::operator()(const char* buf) {
      for (int i = 0; i < size_arr; i++) {
        if (strncmp(buf, ptr[i], strlen(buf)) != 0) {
          for (int j = i+1; j < size_arr; j++) {
            strncpy(ptr[j-1], ptr[j], MAX_WORDLEN-1);
            wipe(ptr[j]);
          }
          i--;
          size_arr--;
        }
      }
      return *this;
    }
    const char* Arr::operator[](int index) {
      return ptr[index-1];
    }
  };
