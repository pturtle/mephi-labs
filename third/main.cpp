#include <iostream>
#include <cstring>
#include "arr.h"
using namespace std;
using namespace nm;

bool is_number(const std::string& s){
    try {
        std::stod(s);
    } catch(...) {
        return false;
    }
    return true;
}

bool get_input(string& line) {
  line.clear();
  if (!(cin >> line)) {
    throw invalid_argument("Invalid input");
    return 1;
  }
  return 0;
}

void menu() {
  cout << "----------------------" << endl;
  cout << "1. Initialize array" << endl;
  cout << "2. Print array" << endl;
  cout << "3. Get element by index" << endl;
  cout << "4. Find element" << endl;
  cout << "5. Add element" << endl;
  cout << "6. Sort array" << endl;
  cout << "7. Make an array starting with..." << endl;
  cout << "8. Exit" << endl;
  cout << "----------------------" << endl;
  cout << "Go on with your choice:" << endl;
}

int main() {
  char** ptr = new char* [MAX_ARRLEN];
  Arr* arr = NULL;
  int temp = 0;
  std::string line;
  for (;;) {
    menu();
    get_input(line);
    if (!is_number(line)){
      continue;
    }
    switch (stoi(line)) {
      case 1:
        cout << "a) By default" << endl;
        cout << "b) With one word" << endl;
        cout << "c) With few words" << endl;
        line.clear();
        cin >> line;
        switch (line.c_str()[0]) {
          case 'a':
            arr = new Arr();
            break;
          case 'b':
            get_input(line);
            arr = new Arr((char*)line.c_str(), (int)line.length());
            break;
          case 'c':
            cout << "Input number of words:" << endl;
            get_input(line);
            temp = stoi(line);
            temp = temp > MAX_ARRLEN ? MAX_ARRLEN : temp;
            cout << "Waiting for your " << temp << " words" << endl;
            for (int i = 0; i < temp; i++) {
              cin.width(20);
              ptr[i] = new char[MAX_WORDLEN];
              cin >> ptr[i];
            }
            arr = new Arr(ptr, temp);
            for (int i = 0; i < temp; i++) {
              delete[] ptr[i];
              ptr[i] = NULL;
            }
            break;
        }
        break;
      case 2:
        if (arr) cout << *arr;
        break;
      case 3:
        if (!arr) break;
        cout << "Input index>> ";
        get_input(line);
        temp = stoi(line);
        if (temp > arr->len() || temp < 1) {
          break;
        } else {
          cout << "[+] Your word is " << (*arr)[temp] << endl;
        }
        break;
      case 4:
        if (!arr) break;
        cout << "What to find>> ";
        get_input(line);
        temp = arr->find(line.c_str());
        temp == -1 ? cout << "[-] Not found" << endl : cout << endl << "[+] Index "<< temp+1 << endl;
        break;
      case 5:
        if (!arr) break;
        cout << "What to add>> ";
        get_input(line);
        *arr += line.c_str();
        break;
      case 6:
        if (!arr) break;
        arr->sort();
        cout << "Sorting done." << endl;
        break;
      case 7:
        if (!arr) break;
        cout << "Input the start you want>> ";
        get_input(line);
        (*arr)(line.c_str());
        break;
      case 8:
        exit(0);
    }
  }
}
