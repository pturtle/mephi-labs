#include <ostream>
#define MAX_WORDLEN 20
#define MAX_ARRLEN 20

namespace nm {
  class Arr {
  private:
    int size_arr = 0;
    char ptr[MAX_ARRLEN][MAX_WORDLEN];
  public:
    Arr();
    Arr(char* buf, int size);
    Arr(char** buf, int size);
    ~Arr();
    int len();
    int find(const char* buf);
    const char* str(int line);
    Arr& sort();
    friend std::ostream& operator<< (std::ostream &out, const Arr &vector);
    Arr &operator += (const char* buf);
    Arr &operator () (const char* buf);
    const char* operator[](int index);
  };
}
