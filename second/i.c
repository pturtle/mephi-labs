#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool is_palindrome(int n) {
    int t, r = 0;
    t = n;
    while (t != 0) {
    r = r * 10;
    r = r + t%10;
    t = t/10;
    }
    if (n == r)
    return true;
    else
    return false;
}

void print(int** ptr, int m) {
    for (int i=0; i<m; i++) {
    	if (*ptr[i]==0) {
    		printf("-");
    	}
    	for (int j = 1; j < *ptr[i]+1; j++) {
    		printf("%d ", ptr[i][j]);
    	}
    	printf("\n");
    }
}


int** fill(int** ptr1, int m) {
    int** ptr2;
    ptr2 = malloc(m*sizeof(int*));
    for (int i=0; i<m; i++) {
    	int count=1;
    	ptr2[i] = malloc((*ptr1[i]+1)*sizeof(int));
    	for (int j = 1; j < *ptr1[i]+1; j++) {
    		if (is_palindrome(ptr1[i][j])) {
    			ptr2[i][count] = ptr1[i][j];
    			count += 1;
    		}
    	}
    	*ptr2[i]=count-1;
    }
    return ptr2;
}


int main() {
    int m=0, n=0;
    int** arr;
    int** arr1;
    printf("Input m (rows)>>");
    scanf("%d", &m);
    arr = malloc(m*sizeof(int*));
    for (int i=0; i<m; i++) {
    	printf("Input n (columns)>>");
    	scanf("%d", &n);
    	arr[i] = malloc((n+1)*sizeof(int));
    	for (int j=1; j<n+1; j++) {
    		printf("Go on>>");
    		scanf("%d", (arr[i]+j));
    	}
    	*arr[i] = n;
    }
    printf("\nOriginal matrix:\n");
    print(arr, m);
    arr1 = fill(arr, m);
    printf("\nMatrix of palindromes:\n");
    print(arr1, m);
}
